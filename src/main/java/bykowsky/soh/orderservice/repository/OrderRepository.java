package bykowsky.soh.orderservice.repository;

import bykowsky.soh.orderservice.entity.Order;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends ListCrudRepository<Order, Integer> {
}
