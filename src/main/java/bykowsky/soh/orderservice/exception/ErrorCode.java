package bykowsky.soh.orderservice.exception;

public enum ErrorCode {
    PRODUCT_NOT_FROUND,
    INSUFFICIENT_QUANTITY,
    INTERNAL_SERVER_ERROR
}
