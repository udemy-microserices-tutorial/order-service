package bykowsky.soh.orderservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class OrderServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(OrderServiceException.class)
    public ResponseEntity<OrderServiceErrorResponse> handleProductServiceException(OrderServiceException ex) {

        OrderServiceErrorResponse errorResponse = OrderServiceErrorResponse.builder()
                .errorMessage(ex.getMessage())
                .errorCode(ex.getErrorCode().toString())
                .build();

        log.debug("Handling OrderServiceException: {}", ex);
        return new ResponseEntity<>(errorResponse, getHttpStatus(ex));
    }

    private static HttpStatus getHttpStatus(OrderServiceException ex) {
        HttpStatus httpStatus;
        switch (ex.getErrorCode()) {
            case PRODUCT_NOT_FROUND -> httpStatus = HttpStatus.NOT_FOUND;
            case INSUFFICIENT_QUANTITY -> httpStatus = HttpStatus.NOT_ACCEPTABLE;
            default -> httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return httpStatus;
    }
}
