package bykowsky.soh.orderservice.exception;

import lombok.Data;

@Data
public class OrderServiceException extends RuntimeException {
    private ErrorCode errorCode;

    public OrderServiceException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}
