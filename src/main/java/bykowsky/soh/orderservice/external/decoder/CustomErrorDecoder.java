package bykowsky.soh.orderservice.external.decoder;

import bykowsky.soh.orderservice.exception.ErrorCode;
import bykowsky.soh.orderservice.exception.OrderServiceException;
import bykowsky.soh.orderservice.external.response.ProductServiceErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class CustomErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        ObjectMapper objectMapper = new ObjectMapper();

        log.debug("Decoding feign exception and rethrowing as OrderServiceException");
        log.debug("Url: [{}]", response.request().url());
        log.debug("Headers: [{}]", response.headers());
        log.debug("Body: [{}]", response.body());

        try {
            ProductServiceErrorResponse errorResponse = objectMapper.readValue(
                    response.body().asInputStream(), ProductServiceErrorResponse.class);
            return new OrderServiceException(errorResponse.getErrorMessage(),
                    ErrorCode.valueOf(errorResponse.getErrorCode()));
        } catch (IOException e) {
            throw new OrderServiceException("Internal server error", ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}
