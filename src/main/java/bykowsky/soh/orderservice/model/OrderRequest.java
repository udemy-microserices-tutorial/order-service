package bykowsky.soh.orderservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {

    private Integer productId;
    private Integer quantity;
    private Integer totalAmount;
    private PaymentMode paymentMode;
}
