package bykowsky.soh.orderservice.service;

import bykowsky.soh.orderservice.model.OrderRequest;

public interface OrderService {
    long placeOrder(OrderRequest orderRequest);
}
