package bykowsky.soh.orderservice.service;

import bykowsky.soh.orderservice.entity.Order;
import bykowsky.soh.orderservice.external.client.ProductService;
import bykowsky.soh.orderservice.model.OrderRequest;
import bykowsky.soh.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    private final ProductService productService;
    @Override
    public long placeOrder(OrderRequest orderRequest) {
        log.info("Received placeOrder request [{}]", orderRequest);

        log.info("Calling product service to reduce product quantity");
        productService.reduceQuantity(orderRequest.getProductId(), orderRequest.getQuantity());

        log.info("Creating order with status: CREATED");
        Order order = Order.builder()
                .productId(orderRequest.getProductId())
                .totalAmount(orderRequest.getTotalAmount())
                .status("CREATED")
                .date(Instant.now())
                .quantity(orderRequest.getQuantity())
                .build();

        order = repository.save(order);
        log.info("Order places successfully with orderId [{}]", order.getOrderId());
        return order.getOrderId();
    }
}
